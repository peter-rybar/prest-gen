import { HElements } from "peryl/dist/hsml";

export function hello(name: string): HElements {
    return [
        ["p", [
            ["input.w3-input", { type: "text", value: name }],
            ["p", [
                "Hello ", ["strong", [name]], " !"
            ]]
        ]]
    ];
}
